package com.ecommerce.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.ecommerce.pojo.JwtAuthenticationToken;
import com.ecommerce.pojo.SecurityUserDetails;
import com.ecommerce.pojo.User;
import com.ecommerce.service.UserService;
import com.ecommerce.util.CustomException;

import java.util.List;
import java.util.Optional;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private JwtValidator validator;
    
    @Autowired
    private UserService userService;
    
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
    	User user = (User) userDetails;
    	if (user.getActive() == 0) {
            throw new AccountNotConfirmedException("Account is not confirmed yet.");
        }
    }

    public static class AccountNotConfirmedException extends AuthenticationException {
        public AccountNotConfirmedException(String message) {
            super(message);
        }
    }
    
    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) usernamePasswordAuthenticationToken;
        String token = jwtAuthenticationToken.getToken();
        
        List<String> list = validator.validate(token);
        if (list == null || list.size()!=2) {
        	System.out.println("JWT Token is incorrect");
        	throw new CustomException("JWT Token is incorrect");
        }
        
        Optional<User> optinalUser = userService.getUserById(Long.parseLong(list.get(1)));
        optinalUser.orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        SecurityUserDetails user = optinalUser.map(SecurityUserDetails::new).get();
        
        if (user == null) {
        	System.out.println("User not present");
        	throw new CustomException("User not present");
        }

        return user;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return (JwtAuthenticationToken.class.isAssignableFrom(aClass));
    }
}