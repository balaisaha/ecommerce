package com.ecommerce.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import com.ecommerce.pojo.User;

@Component
public class JwtGenerator {

    public String generate(User user) {
    	
        Claims claims = Jwts.claims()
                			.setSubject(user.getName());
        claims.put("id", String.valueOf(user.getId()));
        
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, "ecommerce")
                .compact();
    }
}