package com.ecommerce.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class JwtValidator {

    private String secret = "ecommerce";

    public List<String> validate(String token) {
    	
    	List<String> list = new ArrayList<String>();
        String userID = null;
        String username = null;
        
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
            
            username = body.getSubject();
            userID = (String) body.get("id");
            list.add(username);
            list.add(userID);
        }
        catch (Exception e) {
            System.out.println(e);
        }

        return list;
    }
}