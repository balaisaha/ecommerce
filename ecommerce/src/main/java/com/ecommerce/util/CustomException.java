package com.ecommerce.util;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class CustomException extends AuthenticationException  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2693109705177555453L;
	
	public CustomException(String exception) {
		super(exception);
	}
}