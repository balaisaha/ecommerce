package com.ecommerce.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.pojo.Product;
import com.ecommerce.repository.ProductRepository;

@RestController
@RequestMapping("/api")
public class ProductController {
	
	@Autowired
	private ProductRepository productRepo;
	
	@GetMapping("/products")
	public List<Product> getAllProduct(){
		List<Product> listProduct = productRepo.findAll();
		return listProduct;
	}
	
	@PostMapping("/products")
	public ResponseEntity<String> addProduct(@RequestBody Product product, HttpServletRequest request, HttpServletResponse response) {
		HttpStatus status = null;

		if(product != null) {
			if(productRepo.save(product)!=null)
				return new ResponseEntity<> ("Product Saved.", status.CREATED);
			else
				return new ResponseEntity<> ("An error occured! Please try again.", status.INTERNAL_SERVER_ERROR);
		} else {
				return new ResponseEntity<> ("Ivalid Product data.", status.BAD_REQUEST);
		}
		
	}
	
}
