package com.ecommerce.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.pojo.User;
import com.ecommerce.security.JwtGenerator;
import com.ecommerce.service.UserService;

@RestController
public class TokenController {

	@Autowired
	private UserService userService;
    private JwtGenerator jwtGenerator;

    public TokenController(JwtGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }

    @PostMapping("/token")
    public ResponseEntity<String> generate(@RequestBody final User user, HttpServletRequest request, HttpServletResponse response) {
    	
    	Optional<User> optionalUser = userService.getUserByEmail(user.getEmail());
		
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<> ("Invalid Credentials!", HttpStatus.UNAUTHORIZED);
		}
		
		User resource = optionalUser.get();
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();  
		
		if(!encoder.matches(user.getPassword(),resource.getPassword())) {
			return new ResponseEntity<> ("Invalid Credential!", HttpStatus.UNAUTHORIZED);
		}
		
        String responseToken =  jwtGenerator.generate(resource);
        return new ResponseEntity<String> (responseToken, HttpStatus.OK);
    }    
}