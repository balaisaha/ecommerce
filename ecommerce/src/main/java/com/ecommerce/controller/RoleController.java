package com.ecommerce.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.pojo.Role;
import com.ecommerce.service.RoleService;

@RestController
@RequestMapping("/api")
public class RoleController {
	 
	 @Autowired 
	 RoleService roleService;
	 
	 @PostMapping("/roles")
	 public String addRole(@RequestBody Role role) {
		 if(roleService.addRole(role)){
			 return "Role saved Succesfully!";
		 }
		 return "Failed to save Role.";
	 }
	 
	 @GetMapping("/roles")
	 public List<Role> getRoleList() {
		 List<Role> roles = roleService.findAllRole();
		 return roles;
	 }	
	 
	 @GetMapping("/roles/{id}")
	 public Role getRole(@PathVariable(value="id") Long id){
		Optional<Role> role = roleService.getRoleById(id);
		 
		if(role==null)
			return null;

		Role resource = role.get();
		return resource;
	 }
}
