package com.ecommerce.service;

import java.util.List;
import java.util.Optional;

import com.ecommerce.pojo.Role;
import com.ecommerce.pojo.User;

public interface UserService {
	public boolean addUser(String email, String name, String passeord, Role role);
	public Long addUser(User user);
	public Optional<User> getUserByEmail(String email);
	public Optional<User> getUserById(Long id);
	public List<User> findAllUser();
}
