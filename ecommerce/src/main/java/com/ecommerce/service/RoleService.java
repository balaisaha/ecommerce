package com.ecommerce.service;

import java.util.List;
import java.util.Optional;
import com.ecommerce.pojo.Role;

public interface RoleService {
	public boolean addRole(Role role);
	public Optional<Role> getRoleById(Long id);
	public List<Role> findAllRole();
}
