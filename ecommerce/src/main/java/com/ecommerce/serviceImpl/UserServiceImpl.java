package com.ecommerce.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ecommerce.pojo.Role;
import com.ecommerce.pojo.User;
import com.ecommerce.repository.RoleRepository;
import com.ecommerce.repository.UserRepository;
import com.ecommerce.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	RoleRepository roleRepo;
	
	@Override
	public Long addUser(User user) {
		if(user!=null) {
			String pwd = user.getPassword();
			user.setPassword(new BCryptPasswordEncoder().encode(pwd));
			User user1 = userRepo.save(user);
			if(user1!=null)
				return user1.getId();
		}
			
		return -1L;
	}
	
	@Override
	public List<User> findAllUser() {
		return userRepo.findAll();
	}
	
	@Override
	public Optional<User> getUserById(Long id) {
		return userRepo.findById(id);
	}

	@Override
	public Optional<User> getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return userRepo.findByEmail(email);
	}

	@Override
	public boolean addUser(String email, String name, String passeord, Role role) {
		// TODO Auto-generated method stub
		return false;
	}
}
