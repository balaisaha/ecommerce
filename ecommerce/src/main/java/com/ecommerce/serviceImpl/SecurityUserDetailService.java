package com.ecommerce.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ecommerce.pojo.SecurityUserDetails;
import com.ecommerce.pojo.User;
import com.ecommerce.service.UserService;

@Service
public class SecurityUserDetailService implements UserDetailsService{
	@Autowired
	UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> user = userService.getUserByEmail(email);
		user.orElseThrow(() -> new UsernameNotFoundException("Username not found"));
		
		return user.map(SecurityUserDetails::new).get();
	}
}
