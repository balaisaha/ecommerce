package com.ecommerce.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.pojo.Role;
import com.ecommerce.repository.RoleRepository;
import com.ecommerce.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService{
	@Autowired
	RoleRepository roleRepo;
	
	@Override
	public boolean addRole(Role role) {
		if(role!=null && roleRepo.save(role)!=null)
			return true;
		return false;
	}

	@Override
	public Optional<Role> getRoleById(Long id) {
		return roleRepo.findById(id);
	}
	
	@Override
	public List<Role> findAllRole() {
		return roleRepo.findAll();
	}
	
}
