package com.ecommerce.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class OrderHistory extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2882989718264350443L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="order_id")
	private Long id;
	
	@OneToOne
	@JoinColumn(name="user_id")
	private User user;
}
