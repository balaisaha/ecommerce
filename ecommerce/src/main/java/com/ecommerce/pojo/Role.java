package com.ecommerce.pojo;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "role")
@Data
public class Role extends BaseEntity{

    /**
	 * 
	 */
	private static final long serialVersionUID = 888759393028081237L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    private Long id;
    
    @Column(name = "role")
    private String role;
}